#####################
RWS Datalab Templates
#####################

Documentation
-------------

Full project documentation is available `here <https://rwsdatalab.gitlab.io/public/templates/rws-datalab-templates/>`_.

.. begin-inclusion-intro-marker-do-not-remove

This package contains a `Cookiecutter <https://cookiecutter.readthedocs.io>`_ template to generate an empty project (Python/Svelte/etc.) according to standards used by RWS Datalab.

.. end-inclusion-intro-marker-do-not-remove

.. begin-inclusion-overview-marker-do-not-remove

Overview
--------

The RWS Datalab utilizes a set of standards for developing software. To enforce
these standards, we have created a `Cookiecutter <https://cookiecutter.readthedocs.io>`_ template to generate an empty
project in line with these standards. The template is available in various different
programming languages such as Python and Svelte.

The features of this template include:

.. raw:: latex

    \begin{itemize}
    \item {}
    \sphinxAtStartPar
    Boilerplate tests and documentation

    \item {}
    \sphinxAtStartPar
    \sphinxhref{{{cookiecutter.__project_slug}}/pyproject.toml}{Python setup configuration}

    \item {}
    \sphinxAtStartPar
    Open source software license

    \item {}
    \sphinxAtStartPar
    Code style checking via \sphinxhref{{{cookiecutter.__project_slug}}/.pre-commit-config.yaml}{pre\sphinxhyphen{}commit}

    \item {}
    \sphinxAtStartPar
    \sphinxhref{{{cookiecutter.__project_slug}}/.gitlab-ci.yml}{GitLab CI/CD integration}

    \item {}
    \sphinxAtStartPar
    \sphinxhref{{{cookiecutter.__project_slug}}/.editorconfig}{Editorconfig}

    \item {}
    \sphinxAtStartPar
    Miscellaneous files, such as \sphinxhref{{{cookiecutter.__project_slug}}/CHANGELOG.rst}{Changelog}

    \item {}
    \sphinxAtStartPar
    A \sphinxhref{{{cookiecutter.__project_slug}}/README.rst}{README}

    \end{itemize}

    \iffalse

* Boilerplate tests and documentation
* `Python setup configuration <{{cookiecutter.__project_slug}}/pyproject.toml>`_
* Open source software license
* Code style checking via `pre-commit <{{cookiecutter.__project_slug}}/.pre-commit-config.yaml>`_
* `GitLab CI/CD integration <{{cookiecutter.__project_slug}}/.gitlab-ci.yml>`_
* `Editorconfig <{{cookiecutter.__project_slug}}/.editorconfig>`_
* Miscellaneous files, such as `Changelog <{{cookiecutter.__project_slug}}/CHANGELOG.rst>`_
* A `README  <{{cookiecutter.__project_slug}}/README.rst>`_

.. raw:: latex

    \fi

Project types
"""""""""""""
The four different project types available in the Cookiecutter setup are Python, datascience, Documentation and Svelte.
The project type determines which set of files, tests & documentation is generated.

python
^^^^^^
The *python* project type generates an empty Python package according to standards used by RWS Datalab. This includes basic infrastructure to run tests and create useful documentation.

datascience
^^^^^^^^^^^^
The *datascience* project type is an extension to (and includes) the features offered by the *Python* project type. It adds extra documentation and several directories relevant for datascience projects.

The extra directories are data, models and notebooks. These can be found in the root directory of the generated project. Note that although the *data* directory is part of the Git repository, its contents are ignored by Git. Data will not be (nor should it be!) pushed to Git.

documentation
^^^^^^^^^^^^^
Unlike the previous two project types *documentation* is meant for projects that involve little to no code, such as guides & reports. It offers no features to facilitate the writing, running or testing of code. Note that both *python* and *datascience* projects do also include documentation.

svelte
^^^^^^
The *svelte* project type generates an empty SvelteKit project according to standards used by RWS Datalab.
This includes basic infrastructure to run tests and StoryBook for documentation.

.. end-inclusion-overview-marker-do-not-remove

.. begin-inclusion-usage-marker-do-not-remove

Cookiecutter Installation
"""""""""""""""""""""""""

We recommend developing your software in an isolated Python environment and
assume you are familiar with either **virtualenv + pip** or **conda**.


Step 1: Install ``cookiecutter``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We recommend installing cookiecutter outside the virtual environment you will
be using for developing your software. This way, you don't have to install
cookiecutter for every new project.


If you are using **conda**\ :

.. code-block:: bash

    conda install -c conda-forge cookiecutter


If you are using **virtualenv + pip**\ :

.. code-block:: bash

    pip install --user cookiecutter


This Cookiecutter template now requires Cookiecutter '2.0.1' or higher to function.
You may need to upgrade before using it.

If you are using **conda**\ :

.. code-block:: bash

    conda update -c conda-forge cookiecutter


If you are using **virtualenv + pip**\ :

.. code-block:: bash

    pip install --user cookiecutter --upgrade

.. warning::

  On Windows you must ensure your environment supports `symlinks <https://en.wikipedia.org/wiki/Symbolic_link>`_. To do so, `enable Developer mode in Windows settings <https://learn.microsoft.com/en-us/windows/apps/get-started/enable-your-device-for-development#activate-developer-mode>`_ and configure Git to enable them:

  .. code-block:: bash

      git config --global core.symlinks true

Step 2: Create and activate a Python environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

  If you installed ``cookiecutter`` globally (not in a virtual environment), you can skip this step.

If you are using **virtualenv + pip**\ :

.. code-block:: bash

    $ virtualenv -p python3 env_NEW
    $ . env_NEW/bin/activate


If you are using **conda**\ , :

.. code-block:: bash

    $ conda create -n env_NEW python=3
    $ conda activate env_NEW

.. warning::

  On Windows use an Anaconda prompt to activate the conda environment if Anaconda is not in your PATH.

.. end-inclusion-usage-marker-do-not-remove

Step 3: Generate the files and directory structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We've created separate documentation pages for each project type.
Please refer to the documentation for the project type you want to create.

- `SvelteKit <https://rwsdatalab.gitlab.io/public/templates/rws-datalab-templates/templates/javascript/sveltekit.html#>`_
- `Python <https://rwsdatalab.gitlab.io/public/templates/rws-datalab-templates/templates/python/python.html>`_
- `Datascience <https://rwsdatalab.gitlab.io/public/templates/rws-datalab-templates/templates/python/datascience.html>`_
- `Documentation <https://rwsdatalab.gitlab.io/public/templates/rws-datalab-templates/templates/python/documentation.html>`_

.. begin-inclusion-license-marker-do-not-remove

License
-------
Copyright (c) 2019-2025, Rijkswaterstaat


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


.. end-inclusion-license-marker-do-not-remove
