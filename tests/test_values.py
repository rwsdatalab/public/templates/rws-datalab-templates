# -*- coding: utf-8 -*-

import os
import sys

import pytest

try:
    import sh
except ImportError:
    pass


@pytest.mark.skipif(
    sys.platform.startswith("win"), reason="Skipping test with sh-module on Windows"
)
@pytest.mark.parametrize(
    "template, ctx",
    [
        (
            "python",
            {"project_short_description": "'single quotes'", "author": "Mr. O'Keeffe"},
        ),
        (
            "datascience",
            {"project_short_description": "'single quotes'", "author": "Mr. O'Keeffe"},
        ),
    ],
)
def test_single_double_quotes_in_name_and_description(cookies, template, ctx):
    baked_cookies = cookies.bake(extra_context=ctx, template=("templates/" + template))

    assert baked_cookies.exit_code == 0

    with open(os.path.join(str(baked_cookies.project_path), "pyproject.toml")) as f:
        pyproject = f.read()

    cwd = os.getcwd()
    os.chdir(str(baked_cookies.project_path))

    try:
        os.environ["PIP_INDEX_URL"] = "https://pypi.python.org/simple"
        sh.python(["-m", "build"])
    except sh.ErrorReturnCode as e:
        pytest.fail(e)
    finally:
        os.chdir(cwd)
