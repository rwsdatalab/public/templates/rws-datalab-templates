# -*- coding: utf-8 -*-

import os
import pathlib
import sys

import pytest


current_path = pathlib.Path(os.path.abspath(__file__))
template_dir = current_path.parents[1] / "templates/common/hooks"

sys.path.append(str(template_dir))

import post_gen_project
import pre_gen_project


def test_post_gen():
    post_gen_project.main()
