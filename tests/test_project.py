# -*- coding: utf-8 -*-

import os
import pathlib
import subprocess
import sys
from typing import Set, Union

import pytest
from pytest_cookies.plugin import Cookies

try:
    import sh
except ImportError:
    pass


test_data = pathlib.Path(__file__).parents[1] / "test_data"
template_dir = pathlib.Path(__file__).parents[1] / "templates"


def _patch_sphinx_conf_py(package_name):
    """Remove dynamic versioning from conf.py"

    We don't want to install the package to get versioning within
    the tests, therefore we patch the Sphinx conf.py configuration
    to have static versioning.
    """
    fin = open("conf.py", "rt")
    data = fin.read()
    data = data.replace(f"{package_name}.__version__", '"0.1"')
    data = data.replace(f"import {package_name}", "")
    fin.close()
    fin = open("conf.py", "wt")
    fin.write(data)
    fin.close()


@pytest.mark.parametrize(
    "template,name",
    [
        ("python", "python"),
        ("datascience", "datascience"),
        ("documentation", "documentation"),
    ],
)
def test_project(cookies, template, name):
    baked_cookies = cookies.bake(template=str(template_dir / template))

    assert baked_cookies.exit_code == 0
    assert baked_cookies.exception is None
    assert pathlib.PurePath(baked_cookies.project_path).name == name
    assert pathlib.Path(baked_cookies.project_path).is_dir()


@pytest.mark.skipif(
    sys.platform.startswith("win"), reason="Skipping test with sh-module on Windows"
)
@pytest.mark.parametrize("template", ["python", "datascience"])
def test_package_build(cookies, template):
    baked_cookies = cookies.bake(template=str(template_dir / template))

    assert baked_cookies.exit_code == 0
    assert baked_cookies.exception is None
    cwd = os.getcwd()
    os.chdir(baked_cookies.project_path)

    with open(os.path.join(str(baked_cookies.project_path), "pyproject.toml")) as f:
        pyproject = f.read()

    try:
        os.environ["PIP_INDEX_URL"] = "https://pypi.python.org/simple"
        sh.python(["-m", "build"])
    except sh.ErrorReturnCode as e:
        pytest.fail(e)
    finally:
        os.chdir(cwd)


@pytest.mark.skipif(
    sys.platform.startswith("win"), reason="Skipping test with sh-module on Windows"
)
@pytest.mark.parametrize("template", ["python", "datascience"])
def test_running_tests(cookies, template):
    baked_cookies = cookies.bake(template=str(template_dir / template))

    assert baked_cookies.exit_code == 0
    assert baked_cookies.exception is None

    cwd = os.getcwd()
    os.chdir(str(baked_cookies.project_path))

    try:
        sh.python(["-m", "pytest"])
    except sh.ErrorReturnCode as e:
        pytest.fail(e)
    finally:
        os.chdir(cwd)


@pytest.mark.skipif(
    sys.platform.startswith("win"), reason="Skipping test with sh-module on Windows"
)
@pytest.mark.parametrize("template", ["python", "datascience"])
def test_building_documentation_no_apidocs(cookies, template):
    baked_cookies = cookies.bake(template=str(template_dir / template))

    assert baked_cookies.exit_code == 0
    assert baked_cookies.exception is None

    cwd = os.getcwd()
    os.chdir(os.path.join(baked_cookies.project_path, "doc"))

    try:
        _patch_sphinx_conf_py(template)
        os.chdir(baked_cookies.project_path)
        subprocess.check_call(["make", "--directory=doc", "html"])
    except sh.ErrorReturnCode as e:
        pytest.fail(e)
    finally:
        os.chdir(cwd)


@pytest.mark.skipif(
    sys.platform.startswith("win"), reason="Skipping test with sh-module on Windows"
)
@pytest.mark.parametrize("template", ["python", "datascience"])
def test_building_documentation_apidocs(cookies, template):
    baked_cookies = cookies.bake(
        extra_context={"apidoc": "yes"}, template=str(template_dir / template)
    )

    assert baked_cookies.exit_code == 0
    assert baked_cookies.exception is None

    cwd = os.getcwd()
    os.chdir(os.path.join(baked_cookies.project_path, "doc"))

    try:
        _patch_sphinx_conf_py(template)
        os.chdir(baked_cookies.project_path)
        subprocess.check_call(["make", "--directory=doc", "html"])
    except sh.ErrorReturnCode as e:
        pytest.fail(e)
    finally:
        os.chdir(cwd)

    apidocs = pathlib.PurePosixPath(
        baked_cookies.project_path, "doc", "_build", "html", "apidocs"
    )
    assert pathlib.Path(pathlib.PurePosixPath(apidocs, f"{template}.html")).is_file()
    assert pathlib.Path(
        pathlib.PurePosixPath(apidocs, f"{template}.example.html")
    ).is_file()


def get_list_of_files(
    path: Union[pathlib.Path, str], relative_path: bool = True
) -> Set[str]:
    """Recursively get list of files under path.

    Parameters
    ----------
    path
        Path to get list of files from.
    relative_path
        True if list of files are return as relative path. If False, list of files are
        returned as absolute path.

    Returns
    -------
    Set[str]
        Set with list of files under input path.
    """
    filenames = {
        os.path.join(root, file) for root, _, files in os.walk(path) for file in files
    }
    if not relative_path:
        return filenames
    return {os.path.relpath(filename, path) for filename in filenames}


@pytest.mark.parametrize(
    "template",
    ["python", "datascience", "documentation"],
)
def test_expected_files(cookies: Cookies, template: str) -> None:
    """Verify all expected files are present in generated structure.

    There should be no empty directories, all have at least a .gitkeep file.
    Therefore, it is not needed to verify if all directories are present as
    this is indirectly accomplished by checking the files.

    Parameters
    ----------
    cookies
        Object to provide convenient access to the Cookiecutter API.
    template
        Name of the Cookiecutter template to test.
    """
    baked_cookies = cookies.bake(template=("templates/" + template))
    assert baked_cookies.exit_code == 0
    assert baked_cookies.exception is None
    assert pathlib.Path(baked_cookies.project_path).is_dir()
    file_names = get_list_of_files(baked_cookies.project_path)
    file_name = os.path.join(test_data, "expected_files", template + ".txt")
    with open(file_name) as f:
        file_list = f.read().splitlines()
    assert set(file_list) == file_names
