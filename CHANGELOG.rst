Changelog
=========

All notable changes to this project will be documented in this file.

[0.10.0]
--------

Changed
"""""""

* Removed Safety integration due to conflicts with Pydantic


[0.9.3]
-------

Fixed
"""""

* Mypy by removing additional_dependency "types-all" from pre-commit


[0.9.2]

Added
"""""

* Features to the Svelte template
    - Add lint-staged so that only files in staging are linted
    - Add an editorconfig
    - Add jsdom as the vitest environment (this environment has more capabilities than the default environment)

Changed
"""""""

* Improvements to the Svelte template
    - Version bump of dev dependencies
    - Split out the rijkshuisstijl-kleuren into a separate JSON file so that both JS and CSS can access it.
    - Update ESlint config to resolve teething problems we discovered in our first project using the template.
    - Update components directory from "src/components" to "src/stories/components" to promote colocation of stories and component files.
    - Update TailwindCSS config to be more inline with the Rijkshuisstijl.


[0.9.1]

Changed
"""""""

* CI pipeline to RWS Datalab CI templates.


[0.9.0]

Added
"""""

* SvelteKit template "svelte".

[0.8.1]

Changed
"""""""

* `Pylint <https://pypi.org/project/pylint/>`_ to require a minimal score of 8.
* minimal Python version required from 3.7 to `3.10 <https://www.python.org/downloads/release/python-3100/>`_.


[0.8.0]

Changed
"""""""

* dev and doc requirements to be integrated into pyproject.toml, rather than separate txt files.


Removed
"""""""

* sphinxcontrib.pdfembed from all template requirements.


[0.7.1]

Fixed
"""""

* Flake8 settings of created project was missing ignore and select codes.


[0.7.0]
-------

Changed
"""""""

* setup.py replaced by implementing pyproject.toml, in accordance with `PEP-0518 <https://peps.python.org/pep-0518/>`_.


Fixed
"""""

* Documentation generation for Sphinx 7.


Removed
"""""""

* Safety testing from pre-commit, due issues with Setuptools. It will still be run in the pipeline.


[0.6.4]
-------

Fixed
"""""

* Removed Sphinx pin to <6. The underlying Pydata compatibility issue has been resolved in `Pydata v0.13.0 <https://github.com/pydata/pydata-sphinx-theme/releases/tag/v0.13.0>`_.


[0.6.3]
-------

Added
"""""

* Added support for `sphinx-autodoc-typehints <https://github.com/tox-dev/sphinx-autodoc-typehints>`_ in generated documentation.


[0.6.2]
-------

Fixed
"""""

* Pipeline Pytest running through Setuptools, as this caused testing issues.


Added
"""""

* `.safety-policy.yml <https://docs.pyup.io/docs/safety-20-policy-file>`_, to be used to describe, potential, ignored Safety issues and display them when testing.


[0.6.1]
-------

Fixed
"""""

* Pytest coverage malfunctioning.


[0.6.0]
-------

Added
"""""

* Apache 2.0 license.


Changed
"""""""

* Renamed from "python-template" to "rws-datalab-templates" and made public.


[0.5.3]
-------

Changed
"""""""

* Changed "data science" project_type name to "datascience" to ease testing.


[0.5.2]
-------

Fixed
"""""

* Merged requirements-dev and requirements-doc, as documentation is integral to the development process.


[0.5.1]
-------

Changed
"""""""

* Replaced restructuredtext-lint with rstcheck to allow linting of more rst files.


[0.5.0]
-------

Added
"""""

* Template directories with symlinks, establishing a clear division between files of various project types.


[0.4.1]
-------

Fixed
"""""

* project_name may only consist of alphanumerical characters and spaces. This is now enforced.

Added
"""""

* Split requirements.txt into requirements-dev.txt and requirements-doc.txt.
* Added acceptance testing script to scripts folder.


[0.4.0]
-------

Important
"""""""""

* *NOW REQUIRES COOKIECUTTER V2.0.1 OR HIGHER TO FUNCTION.*

Added
"""""

* Swapped sphinx-rtd-theme for pydata-sphinx-theme.
* Implemented non-rendered variables for Cookiecutter project_slug and package_name.


[0.3.0]
-------

Fixed
"""""

* `Default documentation structure. <https://gitlab.com/rwsdatalab/public/templates/rws-datalab-templates/-/commit/41433e8f5bfe95973ed7abfcfaad567f1ebc9bbd>`_
* `Set datalab author and e-mail as default. <https://gitlab.com/rwsdatalab/public/templates/rws-datalab-templates/-/commit/514470553ce316a38417d73045978dce41019da0>`_


[0.2.3]
-------

Added
"""""

* Default mypy config in setup.cfg.
* flake8-polyfill dependency to template dev requirements.


[0.2.2]
-------

Fixed
"""""

* Issue #4


[0.2.1]
-------

Added
"""""

* Calculating code quality with pylint + badge.
* Calculating complexity and maintainability with radon (no badge).
* Add badge to Bandit (security).
* Add badge to Safety (security).
* Add badge to Documentation (GitLab pages).
* Drop only/except syntax in favor of new rules syntax in configuration file.


[0.2.0]
-------

Added
"""""

* Add gitlab-ci integration.
* Small bug fixes.


[0.1.0]
-------

Added
"""""

* Initial release of RWS Datalab Templates.
