.. rws-datalab-templates documentation master file, created by
   sphinx-quickstart on Thu Jun 21 11:07:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

RWS Datalab Templates documentation
===================================

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex`.
- Report bugs with rws-datalab-templates in our `issue tracker <https://gitlab.com/rwsdatalab/public/templates/rws-datalab-templates/-/issues>`_.
- See this document as `pdf <rws-datalab-templates.pdf>`_.

.. toctree::
   :maxdepth: 1
   :caption: First steps

   Overview <overview.rst>

Available templates
-------------------

The following templates are available in our cookiecutter template repository.
We distinguish between Python and JavaScript templates.

.. toctree::
   :maxdepth: 1
   :caption: Python Templates

   DataScience template <templates/python/datascience.rst>
   Documentation template <templates/python/documentation.rst>
   Python package template <templates/python/python.rst>

.. toctree::
   :maxdepth: 1
   :caption: JavaScript Templates

   SvelteKit template <templates/javascript/sveltekit.rst>

Extra information
-----------------

This section contains extra information about the rws-datalab-templates such
as the API documentation, how to contribute and license information.

.. toctree::
   :maxdepth: 1
   :caption: API documentation


.. toctree::
   :maxdepth: 1
   :caption: Acceptance

   Acceptance testing <acceptance_testing.rst>


.. toctree::
   :maxdepth: 1
   :caption: All the rest

   Contributing <contributing.rst>
   Release notes <changelog.rst>
   License <license.rst>
