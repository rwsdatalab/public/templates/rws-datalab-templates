##################
Acceptance testing
##################

Overview
""""""""

As part of the acceptance test for every new release of the RWS Datalab Cookiecutter template a number of tests need to be run.
The *scripts* folder of this repository contains a tool to help facilitate this process: *acceptance_tests.py*. It can help you generate test projects, push them to GitLab and refer you to their location, pipelines & documentation.
If the process is successful, these will then need be manually checked. On conclusion, the tool can help you easily delete the test projects both locally and remotely.


Configuration
"""""""""""""

Terminal arguments
^^^^^^^^^^^^^^^^^^

The recommended method for configuring the test suite is to provide information to the application on each execution by providing them as arguments:

.. code-block:: bash

    python acceptance_tests.py --token glpat-thisisanexampletoken --id 12345678

*token* is your `GitLab Personal Access Token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>`_, required to authorize pushing test repositories to GitLab.

*id* is the ID of the `GitLab subgroup <https://docs.gitlab.com/ee/user/group/subgroups/#subgroups>`_ you want to push your test repositories to. This "Group ID" can be found on your group's page on GitLab.com.

Config file
^^^^^^^^^^^

Alternatively, it is possible to set up a configuration file in your home directory.

.. code-block:: bash

    Unix: "/home/<username>/.cookiecutter_acceptance_config"
    Windows: "C:\Users\<username>\.cookiecutter_acceptance_config"

The configuration file should have permissions set to 600 and should contain the following information:

.. code-block:: bash

    token: glpat-thisisanexamplestring
    id: 12345678

If set up properly, no arguments will be required to run acceptance_tests.py.


Usage
"""""

To run the tool, navigate to */scripts/* and run:

.. code-block:: bash

    python acceptance_tests.py

You will be presented with a menu containing various options to generate example projects and navigate to their pipelines & documentation.

Troubleshooting
"""""""""""""""

If you are not able to push to Gitlab using option 3, ensure that the archived projects
are fully deleted. You can do this by going to ``General > Advanced > Remove project`` in the Gitlab UI.
