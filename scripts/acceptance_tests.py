# -*- coding: utf-8 -*-
"""Acceptance tests to assist manual checks of every project_type before a release."""

import argparse
import logging
import os
import shutil
import subprocess  # nosec: B404
import sys
from functools import partial
from pathlib import Path
from typing import Any, Dict

import configargparse
import cookiecutter.main
import git
import gitlab
import pre_commit.main

test_projects = ["python", "datascience", "documentation", "svelte"]

logger = logging.getLogger(__name__)

_testdir = "_acceptance_tests_results"


def _format_logging():
    """Format logging."""
    handlers = [logging.StreamHandler()]
    logging.basicConfig(
        level=logging.INFO,
        format="%(levelname)s: %(message)s",
        handlers=handlers,
    )


def generate_projects(test_group_path: str) -> None:
    """Generate local test projects.

    Parameters
    ----------
    test_group_path
        The path of the test group in your GitLab environment:
        E.g. "rwsdatalab/testing/cookiecutter/"
        for "git@gitlab:rwsdatalab/testing/cookiecutter/python.git"
    """
    logger.info("Generating test projects." + os.linesep)

    for project in test_projects:
        template = cookiecutter.main.load(
            "../../test_data/cookiecutter_replays/", project
        )
        template["cookiecutter"]["gitlab_group"] = test_group_path
        cookiecutter.main.generate_files(
            ("../../templates/" + project + "/"), context=template
        )

    print()


def initiate_local_repositories(test_group_path: str) -> None:
    """Initiate repositories and run pre-commit.

    Parameters
    ----------
    test_group_path
        The path of the test group in your GitLab environment:
        E.g. "rwsdatalab/testing/cookiecutter/"
        for "git@gitlab:rwsdatalab/testing/cookiecutter/python.git"
    """
    logger.info("Initiating local repositories." + os.linesep)

    for project in test_projects:
        if os.path.isdir(project):
            if project == "svelte":
                initiate_local_svelte_repository(test_group_path, project)
            else:
                initiate_local_repository(test_group_path, project)
    print()


def initiate_local_svelte_repository(test_group_path: str, project: str) -> None:
    """Initiate repositories and run pre-commit.

    Parameters
    ----------
    test_group_path
        The path of the test group in your GitLab environment:
        E.g. "rwsdatalab/testing/cookiecutter/"
        for "git@gitlab:rwsdatalab/testing/cookiecutter/python.git"
    project
        The project type
        "svelte"
    """
    logger.info("Initiating " + project + " repository. " + os.linesep)

    os.chdir(project)

    # Needed to create a package-lock.json file
    subprocess.run(["npm", "install"], check=True)  # nosec: B603, B607

    # Initiate git repository
    repository = git.Repo.init(os.getcwd())
    # Needed to display pre-commit output
    type(repository.git).GIT_PYTHON_TRACE = "full"
    repository.git.add(".")
    try:
        repository.git.commit("-m", "initial commit")

    except Exception as e:
        logger.warning("Commit error: " + str(e))

    repository.git.checkout(b="develop")
    repository.git.remote(
        "add",
        "origin",
        ("git@gitlab.com:" + test_group_path + "/" + project + ".git"),
    )
    os.chdir("..")


def initiate_local_repository(test_group_path: str, project: str) -> None:
    """Initiate repositories and run pre-commit.

    Parameters
    ----------
    test_group_path
        The path of the test group in your GitLab environment:
        E.g. "rwsdatalab/testing/cookiecutter/"
        for "git@gitlab:rwsdatalab/testing/cookiecutter/python.git"
    project
        The project type
        "python", "datascience" or "documentation"
    """
    logger.info("Initiating " + project + " repository. " + os.linesep)

    os.chdir(project)

    repository = git.Repo.init(os.getcwd())
    # Needed to display pre-commit output
    type(repository.git).GIT_PYTHON_TRACE = "full"
    pre_commit.main.main(["install"])
    repository.git.add(".")
    try:
        repository.git.commit("-m", "initial commit")

    except Exception as e:
        logger.warning("Commit error: " + str(e))

    repository.git.add(".")
    repository.git.commit("-m", "pre-commit autofixes")
    repository.git.checkout(b="develop")
    repository.git.remote(
        "add",
        "origin",
        ("git@gitlab.com:" + test_group_path + "/" + project + ".git"),
    )
    os.chdir("..")
    print()


def upload_local_projects(test_group_url: str, test_doc_url: str) -> None:
    """Upload local test projects to GitLab.

    Parameters
    ----------
    test_group_url
         URL of the GitLab group to push projects to.
    test_doc_url
        URL where the produced documentation of a project ends up.
    """
    logger.info("Uploading test projects to GitLab test group." + os.linesep)

    for project in test_projects:
        if os.path.isdir(project):
            os.chdir(project)
            repository = git.Repo(os.getcwd())
            repository.git.push("--set-upstream", "origin", "main")
            repository.git.push("--set-upstream", "origin", "develop")
            os.chdir("..")

    print_pipeline_urls(test_group_url, test_doc_url)
    print()


def print_pipeline_urls(test_group_url: str, test_doc_url) -> None:
    """Print URLs of project pipelines for easy access.

    Parameters
    ----------
    test_group_url
         URL of the GitLab group to push projects to.
    test_doc_url
        URL where the produced documentation of a project ends up.
    """
    for project in test_projects:
        logger.info(
            os.linesep
            + project
            + " has been uploaded. Check out the pipelines @ "
            + test_group_url
            + "/"
            + project
            + "/-/pipelines and the documentation @ "
            + test_doc_url
            + project
            + "/"
        )


def delete_remote_projects(gitlab_access: gitlab.Gitlab, test_group_id: int) -> None:
    """Delete remote test projects to GitLab.

    Parameters
    ----------
    gitlab_access
        Object containing credentials that gives access to GitLab API.
    test_group_id
        ID of the GitLab group to push projects to.
    """
    logger.info("Deleting remote test_projects from test group." + os.linesep)

    group = gitlab_access.groups.get(test_group_id)
    for project in group.projects.list(iterator=True):
        if project.name in test_projects:
            gitlab_access.projects.delete(project.id)
            logger.info(project.name + " has been deleted." + os.linesep)

    print()


def delete_local_projects() -> None:
    """Delete local test projects."""
    logger.info("Deleting local projects from GitLab test group." + os.linesep)

    for project in test_projects:
        if os.path.isdir(project):
            shutil.rmtree(project)
            logger.info(project + " has been deleted." + os.linesep)

    print()


def print_menu(
    menu_options: Dict[int, Dict[str, Any]], test_group_id: int, test_group_url: str
) -> None:
    """Print a menu to navigate the various options.

    Parameters
    ----------
    menu_options
        Dictionary containing main menu text, functions & arguments.
    test_group_id
        ID of the GitLab group to push projects to.
    test_group_url
        URL of the GitLab group to push projects to.
    """
    print("Test projects: ", test_projects)
    print("Test group id: ", test_group_id)
    print("Test group url: " + test_group_url + os.linesep)

    for key in menu_options.keys():
        print("Choose", key, "for", menu_options[key]["text"])

    print()


def parse_args() -> argparse.Namespace:
    """Parse the config file for inputs.

    Returns
    -------
    argparse.Namespaces
        The parsed arguments.
    """
    parser = configargparse.ArgumentParser(
        default_config_files=[str(Path.home() / ".cookiecutter_acceptance_config")]
    )
    parser.add_argument(
        "-t",
        "--token",
        type=str,
        required=True,
        dest="gitlab_personal_access_token",
    )
    parser.add_argument(
        "-i",
        "--id",
        type=int,
        required=True,
        dest="test_group_id",
    )
    return parser.parse_args()


def create_test_directory():
    """Create directory for the repositories created from the templates."""
    if not os.path.isdir(_testdir):
        os.mkdir(_testdir)


def main() -> None:
    """Present a menu to step through basic test project generation and deletion."""
    args = parse_args()

    gitlab_access = gitlab.Gitlab(
        private_token=vars(args)["gitlab_personal_access_token"]
    )
    test_group_id = vars(args)["test_group_id"]

    test_group_path = gitlab_access.groups.get(test_group_id).full_path
    test_group_url = "https://gitlab.com/" + test_group_path

    test_group_path_split = test_group_path.partition("/")
    test_doc_url = (
        "https://"
        + test_group_path_split[0]
        + ".gitlab.io/"
        + test_group_path_split[2]
        + "/"
    )

    create_test_directory()

    os.chdir(_testdir)

    # mypy will take issue with partial unless type is declared Callable or Any
    menu_options: Dict[int, Dict[str, Any]] = {
        1: {
            "text": "generating test projects.",
            "function": partial(generate_projects, test_group_path),
        },
        2: {
            "text": "initiating local repositories.",
            "function": partial(initiate_local_repositories, test_group_path),
        },
        3: {
            "text": "uploading test projects to GitLab test group.",
            "function": partial(upload_local_projects, test_group_url, test_doc_url),
        },
        4: {
            "text": "deleting local projects from test group.",
            "function": partial(delete_local_projects),
        },
        5: {
            "text": "deleting remote test projects from test group.",
            "function": partial(delete_remote_projects, gitlab_access, test_group_id),
        },
        6: {"text": "exiting this menu.", "function": partial(sys.exit, 1)},
    }

    while True:
        print_menu(menu_options, test_group_id, test_group_url)

        menu_choice = int(input("Enter your choice: "))

        if menu_choice in menu_options:
            menu_options[menu_choice]["function"]()


if __name__ == "__main__":
    _format_logging()
    main()
