import adapter from "@sveltejs/adapter-static";
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://kit.svelte.dev/docs/integrations#preprocessors
  kit: {
    adapter: adapter(),
    alias: {
      $components: "./src/stories/components",
      $lib: "./src/lib",
      $root: "./",
      $src: "./src",
    },
  },
  // for more information about preprocessors
  preprocess: [vitePreprocess()],
};

export default config;
