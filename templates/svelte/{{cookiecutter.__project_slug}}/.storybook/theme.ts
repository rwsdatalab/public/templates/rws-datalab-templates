import { create } from "@storybook/theming";

const BRAND_INFO = {
  logo: undefined,
  title: "{{cookiecutter.project_name}}",
  url: "?path=/docs/resources-homepage--docs",
};

export const lightTheme = create({
  base: "light",
  brandImage: BRAND_INFO.logo,
  brandTitle: BRAND_INFO.title,
  brandUrl: BRAND_INFO.url,
});

export const darkTheme = create({
  base: "dark",
  brandImage: BRAND_INFO.logo,
  brandTitle: BRAND_INFO.title,
  brandUrl: BRAND_INFO.url,
});
