import type { StorybookConfig } from "@storybook/sveltekit";
import remarkGfm from "remark-gfm";
import type { InlineConfig } from "vite";

/**
 * This is a workaround for the following issue:
 * @see https://github.com/storybookjs/storybook/issues/19873
 * This is necessary to allow the docs folder to be served by vite
 * @param config - The vite config
 * @returns The vite config
 */
// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
function viteFinal(config: InlineConfig): InlineConfig {
  const serverConfig = config.server ?? {};
  const fsConfig = serverConfig.fs ?? {};
  const allowList = fsConfig.allow ?? [];
  const fs = {
    allow: [...allowList, "doc", "rijkskleuren.json"],
    fsConfig,
  };

  return {
    ...config,
    server: {
      ...config.server,
      fs,
    },
  };
}

const storybookConfig: StorybookConfig = {
  addons: [
    {
      name: "@storybook/addon-docs",
      options: {
        configureJSX: true,
        mdxPluginOptions: {
          mdxCompileOptions: {
            remarkPlugins: [remarkGfm],
          },
        },
      },
    },
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-svelte-csf",
    "@storybook/addon-a11y",
    "@storybook/addon-storysource",
    "storybook-dark-mode",
    "@storybook/addon-themes",
  ],
  core: {
    disableTelemetry: true,
  },
  docs: {
    autodocs: "tag",
  },
  framework: {
    name: "@storybook/sveltekit",
    options: {},
  },
  staticDirs: ["../static/"],
  stories: [
    "../doc/**/*.mdx",
    "../doc/**/*.stories.@(js|jsx|mjs|ts|tsx|svelte)",
    "../src/**/*.mdx",
    "../src/**/*.stories.@(js|jsx|mjs|ts|tsx|svelte)",
  ],
  viteFinal,
};
export default storybookConfig;
