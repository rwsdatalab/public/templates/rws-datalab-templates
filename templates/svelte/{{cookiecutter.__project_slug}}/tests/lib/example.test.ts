import { expect, test } from "vitest";

import { sum } from "../../src/lib/example";

test("sum", () => {
  expect(sum(1, 2)).toBe(3);
});
