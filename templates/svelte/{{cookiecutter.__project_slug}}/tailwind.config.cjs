import rijkskleuren from "./rijkskleuren.json";
import * as tailwindSafeArea from "tailwindcss-safe-area";

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,svelte,ts,css,mdx}"],
  darkMode: ["class"],
  theme: {
    colors: {
      ...rijkskleuren,
      titelgrijs: "#333",
      subtitelgrijs: "#888",
    },
    extend: {
      screens: {
        // Checks if the user added this as a PWA to their homescreen
        pwa: { raw: "(display-mode: standalone)" },
      },
      boxShadow: {
        DEFAULT: "0 1px 3px #0006",
      },
      textColor: {
        link: rijkskleuren.donkerblauw.DEFAULT,
        "visited-link": rijkskleuren.paars.DEFAULT,
      },
      borderRadius: {
        DEFAULT: "3px",
      },
      fontFamily: {
        sans: ['"RijksoverheidSansText"', "Verdana", "Arial", "sans-serif"],
        serif: ['"RijksoverheidSerifText"', "Times New Roman", "serif"],
      },
      backgroundImage: {
        "line-hatch":
          "repeating-linear-gradient(129deg, transparent, transparent 8px, #fff 8px, #fff 10px)",
      },
    },
  },
  plugins: [tailwindSafeArea],
};
