# {{cookiecutter.project_name}}

## Coding guidelines

This project is set up according to the RWS Datalab
coding guidelines. Documentation regarding these guidelines can be found at the [RWS Datalab - Methodology Wiki](https://rwsdatalab.gitlab.io/guides/werkwijze/ontwikkelgids/index.html).

## Installation

To install {{cookiecutter.project_name}}, run the following command:

```bash
git clone https://gitlab.com/{{cookiecutter.gitlab_group}}/{{cookiecutter.__project_slug}}.git
cd {{cookiecutter.__project_slug}}
npm i # or pnpm install, bun install, yarn
```

### Starting a development server

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

### Creating a production build

```bash
npm run build # To create the production build
npm run preview # To preview the production build
```

## Documentation

Documentation for this project can be found in the
storybook documentation or if Gitlab CI/CD is enabled.

The storybook documentation can be found at [localhost:6006](localhost:6006) after running the following commands:

```bash
npm run docs:build
npm run storybook
```

## License

{{cookiecutter.license}}
