import type { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
  testDir: "e2e",
  testMatch: /(.+\.)?(spec|test)\.[jt]s/u,
  webServer: {
    command: "npm run build && npm run preview",
    port: 4173,
  },
};

export default config;
