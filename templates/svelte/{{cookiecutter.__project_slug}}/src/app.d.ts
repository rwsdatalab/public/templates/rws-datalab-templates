/**
 * The `App` namespace contains interfaces that are available in your SvelteKit application.
 * @see https://kit.svelte.dev/docs/types#app
 * for information about these interfaces
 * @module
 */
declare global {
  namespace App {
    /*
     * Interface Error {}
     * interface Locals {}
     * interface PageData {}
     * interface Platform {}
     */
  }
}

export {};
