module.exports = {
  root: true,
  parserOptions: {
    project: true,
    extraFileExtensions: [".svelte"],
  },
  extends: [
    "eslint:all",
    "plugin:@microsoft/eslint-plugin-sdl/recommended",
    "plugin:etc/recommended",
    "plugin:perfectionist/recommended-natural",
    "plugin:promise/recommended",
    "plugin:security/recommended",
    "plugin:unicorn/all",
    "plugin:sonarjs/recommended",
    "plugin:regexp/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:tailwindcss/recommended",
    "plugin:svelte/recommended",
    "plugin:jsdoc/recommended-typescript-error",
    "plugin:storybook/recommended",
  ],
  settings: {
    "import/parsers": {
      "@typescript-eslint/parser": [".ts"],
    },
    "import/resolver": {
      typescript: {
        alwaysTryTypes: true,
      },
    },
  },
  overrides: [
    {
      files: ["*.ts"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        parser: "@typescript-eslint/parser",
      },
      rules: {
        "@typescript-eslint/consistent-type-assertions": [
          "error",
          {
            assertionStyle: "as",
            objectLiteralTypeAssertions: "allow-as-parameter",
          },
        ],
      },
    },
    {
      files: ["*.svelte"],
      parser: "svelte-eslint-parser",
      // Parse the `<script>` in `.svelte` as TypeScript by adding the following configuration.
      parserOptions: {
        parser: "@typescript-eslint/parser",
      },
      rules: {
        "no-undef-init": "off",
        "unicorn/no-useless-undefined": "off",
        "unicorn/no-keyword-prefix": "off",
        "unicorn/filename-case": "off",
        "no-magic-numbers": [
          "error",
          {
            ignoreArrayIndexes: true,
            ignoreDefaultValues: true,
            ignoreClassFieldInitialValues: true,
            enforceConst: false,
            detectObjects: false,
          },
        ],
        "@typescript-eslint/consistent-type-assertions": [
          "error",
          {
            assertionStyle: "as",
            objectLiteralTypeAssertions: "allow-as-parameter",
          },
        ],
      },
    },
    /**
     * Remove the following override once this issue is resolved
     * @see https://github.com/storybookjs/storybook/issues/16609
     */
    {
      files: ["*.svelte"],
      rules: {
        "jsdoc/check-tag-names": [
          "error",
          {
            typed: false,
          },
        ],
      },
    },
    // Ignore certain rules in storybook files
    {
      files: ["./.storybook/**/*", "./src/**/*.stories.svelte"],
      rules: {
        "unicorn/prevent-abbreviations": "off",
        "no-invalid-this": "off",
        "no-magic-numbers": "off",
        "no-console": "off",
      },
    },
    {
      files: ["**/*.test.ts"],
      rules: {
        "max-statements": "off",
        "no-plusplus": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "no-magic-numbers": "off",
        "unicorn/no-useless-undefined": "off",
        "perfectionist/sort-objects": "off",
        "sort-keys": "off",
      },
    },
    // Playwright config
    {
      files: ["./playwright.config.ts"],
      rules: {
        "prefer-named-capture-group": "off",
        "security/detect-unsafe-regex": "off",
      },
    },
  ],
  rules: {
    "unicorn/explicit-length-check": "off",
    "consistent-return": ["error", { treatUndefinedAsUnspecified: true }],
    "no-magic-numbers": [
      "error",
      {
        // Ignore 0 for array indexes
        ignore: [0],
        ignoreArrayIndexes: true,
        enforceConst: false,
        ignoreDefaultValues: true,
        ignoreClassFieldInitialValues: true,
        detectObjects: false,
      },
    ],
    "@typescript-eslint/naming-convention": "off",
    /**
     * Prefer using @unicorn/no-null instead. These rules are conflicting.
     *
     * @see https://github.com/sindresorhus/meta/discussions/7
     * @see https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-null.md
     */
    "no-undefined": "off",
    /**
     * Because we're using @unicorn/no-null all non-set and empty values will be set to
     * undefined by default. This is also the default behavior in JavaScript.
     * E.g. the following rule can be turned off.
     */
    "init-declarations": "off",
    "lines-around-comment": "off",
    curly: ["error", "multi-line"],
    "no-ternary": "off",
    "sort-imports": "off", // We're using perfectionist instead
    "one-var": ["error", "never"],
    "func-style": ["error", "declaration", { allowArrowFunctions: true }],
    // Ignore n/no-missing-import rule since it gives false positives
    // "n/no-missing-import": "off",
    complexity: ["error", 7],
    "id-length": [
      "error",
      {
        min: 2,
        // Allow single-letter variable _ for unused variables
        exceptions: ["_"],
        // Allow single-letter i/j/k for loop indices
        // Allow single-letter x/y/z for coordinates
        exceptionPatterns: ["[x-z]", "[i-k]"],
      },
    ],
  },
  ignorePatterns: [
    "*.cjs",
    // Auto-generated files
    ".DS_Store",
    "node_modules",
    "coverage",
    "/.svelte-kit",
    "/build",
    "/package",
    "/storybook-static",
    "/doc",
    // Env files
    ".env",
    ".env.*",
    "!.env.example",
    // # Ignore files for PNPM, NPM and YARN
    "pnpm-lock.yaml",
    "package-lock.json",
    "yarn.lock",
    // Ignore a few config files
    "tsconfig.json",
    "svelte.config.js",
  ],
  env: {
    browser: true,
    es2020: true,
  },
};
