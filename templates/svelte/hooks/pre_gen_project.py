#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Enforces the project slug uses the correct characters and terminates if impossible."""

import logging

import cookiecutter

logger = logging.getLogger(__name__)


def _format_logging():
    """Format logging."""
    handlers = [logging.StreamHandler()]
    logging.basicConfig(
        level=logging.INFO,
        format="%(levelname)s: %(message)s",
        handlers=handlers,
    )


def _validate_cookiecutter_version() -> None:
    """Validate if the environment's Cookiecutter version meets requirements."""
    cookiecutter_min_version_requirement = "2.0.1"
    if cookiecutter.__version__ < cookiecutter_min_version_requirement:
        logger.error(
            "Your Cookiecutter version: '"
            + cookiecutter.__version__
            + "'"
            + " does not meet the minimum requirement of '"
            + cookiecutter_min_version_requirement
            + "."
            + " Please upgrade to meet this requirement."
        )
        exit(1)


def _validate_project_name(project_name: str) -> None:
    """Validate entered project_name.

    Parameters
    ----------
    project_name
        Strong representing the project's name.
    """
    if not project_name.replace(" ", "").isalnum():
        logger.error(
            "The provided project_name '"
            + project_name
            + "' contains special characters."
            + " project_name must be composed solely of alphanumeric and space characters."
        )
        exit(1)


def _validate_project_description(project_description: str) -> None:
    """Validate entered project_description.

    Parameters
    ----------
    project_description
        Strong representing the project's description.
    """
    if project_description != "" and not project_description.replace(" ", "").isalnum():
        logger.error(
            "The provided project_description '"
            + project_description
            + "' contains special characters."
            + " project_description must be composed solely"
            + " of alphanumeric and space characters."
        )
        exit(1)


def _validate_gitlab_path(gitlab_path: str) -> None:
    """Validate the entered gitlab_path.

    Parameters
    ----------
    gitlab_path
        The Gitlab path, e.g. "rwsdatalab/projects/cookiecutter-svelte".
    """
    gitlab_path = gitlab_path.replace(" ", "")
    template = (
        "The gitlab_path must be composed of at least a"
        " Gitlab group and project name separated by a slash.\n"
        "Example: 'rwsdatalab/projects/cookiecutter-svelte'\n"
        "Example: 'myusername/myprojectname'"
    )
    if not gitlab_path:
        logger.error(f"The provided gitlab_path is empty. {template}")
        exit(1)
    if "/" not in gitlab_path.replace(" ", ""):
        logger.error(
            f"The provided gitlab_path '{gitlab_path}' does not contain a slash. {template}"
        )
        exit(1)


def _validate_project_slug(project_name: str, project_slug: str) -> None:
    """Validate resulting __projectslug.

    Parameters
    ----------
    project_name
        Strong representing the project's name.
    project_slug
        The Python package's name.
    """
    if project_slug == "" or not project_slug[0].isalpha():
        logger.error(
            "The provided project_name '"
            + project_name
            + "' resulted in the following invalid __project_slug: "
            + project_slug
            + "."
            + " __project_slug must start with an alphabetical character."
        )
        exit(1)


def main():
    """Enforce the project_slug to be viable and terminate if that is impossible."""
    project_name = "{{cookiecutter.project_name}}"
    project_slug = "{{cookiecutter.__project_slug}}"
    project_description = "{{cookiecutter.project_short_description}}"
    gitlab_group = "{{cookiecutter.gitlab_group}}"

    _validate_cookiecutter_version()
    _validate_project_name(project_name)
    _validate_project_slug(project_name, project_slug)
    _validate_project_description(project_description)
    _validate_gitlab_path(gitlab_group)


if __name__ == "__main__":
    _format_logging()
    main()
