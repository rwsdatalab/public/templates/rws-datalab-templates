#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Customize the template based on project_type."""

# These instructions are printed once Cookiecutter successfully finishes execution.
post_completion_instructions = """
To finish setting up your project:

- Navigate to your project directory.
- Initialize a Git repository.
- Install the required development packages.
- Create your Git branches.
- Add all your files to the Git repository.
- Perform an initial commit.

You could then proceed to add a remote and push it.
For example (you can directly copy these instructions):

cd {{cookiecutter.__project_slug}}
git init
npm i
git checkout -b main
git add .
git commit -m "Initial commit" --no-verify
"""


def main():
    """Customize the template based on project_type."""
    print(post_completion_instructions)


if __name__ == "__main__":
    main()
