AI Impact Assessment
====================

Het AI Impact Assessment (AIIA) wordt gebruikt voor het discussiëren over AI-systemen. Hierbij wordt gekeken naar obstakels in de data, het systeem, de algoritmiek en wordt rekening gehouden met geldende wet- en regelgeving. De AIIA dient als instrument voor gesprek en vastleggen van het denkproces zodat onder andere de verantwoording, kwaliteit en reproduceerbaarheid worden vergroot. Het verwachte resultaat van de AIIA is een helder ingevuld document waarin duidelijk zichtbaar is welke afwegingen gemaakt zijn bij het inzetten van AI in een project.

Primair is de opdrachtgever verantwoordelijk voor het (laten) uitvoeren van de AIIA. Er moet een AIIA worden opgesteld voor elk AI-SYSTEEM. Het invullen van de AIIA gebeurt nadrukkelijk proportioneel, passend bij de impact en het risicoprofiel van de toepassing. De verantwoordelijkheid voor wat proportioneel is, ligt bij de projectleiders en de opdrachtgever.

De proportioneel ingevulde AIIA moet af zijn voor het IN GEBRUIK NEMEN van een AI-systeem. Het is belangrijk dat de AIIA daarna regelmatig wordt bijgesteld, bijvoorbeeld als het doel van het AI-systeem wordt gewijzigd of er veranderingen aan het AI-systeem plaatsvinden. Daarnaast kunnen zich met de loop van de tijd nieuwe risico's voordoen.



Vragenlijst
'''''''''''

De volledige AIIA omvat zo'n 100 vragen. De AIIA is verplicht bij het maken of inkopen van AI-systemen, maar het invullen gebeurt nadrukkelijk proportioneel, door de opdrachtgever en projectleider zelf te bepalen. Dit noemen we: verplicht, maar soepel. Dit betekent vooral dat je met gezond verstand moet nadenken over hoeveel impact jouw AI-SYSTEEM heeft.


Downloaden
''''''''''

De meest actuele versie van de AIIA is, binnen het RWS netwerk, beschikbaar via deze link: `AIIA <https://pleinienw.nl/thoughts/21075>`_
