Dataset
=======

[BESCHRIJF BRON DATASET(S)]


Preprocessing
=============

[BESCHRIJF PRE-PROCESSING STAPPEN]


Input
=====

[BESCHRIJF INPUT DATASET(S) VOOR MODEL]


Representativiteit
''''''''''''''''''

[BESCHRIJF REPRESENTATIVITEIT VOOR INPUT DATASET(S)]


Juistheid en volledigheid
'''''''''''''''''''''''''

[BESCHRIJF DE JUISTHEID EN VOLLEDIGHEID VAN DE GEBRUIKTE DATA]


Labels
''''''

[BESCHRIJF (GENERATIE VAN) GEBRUIKTE LABELS]


Exploratory Data Analysis
=========================
Exploratory Data Analysis (EDA) is een verzameling van technieken dat gericht is om de data te verkennen en begrijpen door de data-gedreven hoofdlijnen te ondervangen. De gebruikte technieken zijn verschillende soorten statistieken en het plotten van de data aan de hand van histogrammen, box plots, scatter plots. Deze stap is belangrijk voordat er gemodelleerd gaat worden omdat het gebruik van modellen een veelvoud aan aannames vereist, denk aan; Is de data goed genoeg opgeschoond? Wat is de verdeling van de data? Zijn er outliers? Is de data scheef verdeeld? Hoeveel classes/categorien zijn er?


Verdeling data
''''''''''''''

[BESCHRIJF VERDELING DATA]


Clustering
''''''''''

[BESCHRIJF CLUSTERING VAN DATA]
