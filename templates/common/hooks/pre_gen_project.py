#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Enforces the project slug uses the correct characters and terminates if impossible."""

import logging

import cookiecutter

logger = logging.getLogger(__name__)


def _format_logging():
    """Format logging."""
    handlers = [logging.StreamHandler()]
    logging.basicConfig(
        level=logging.INFO,
        format="%(levelname)s: %(message)s",
        handlers=handlers,
    )


def _validate_cookiecutter_version() -> None:
    """Validate if the environment's Cookiecutter version meets requirements."""
    cookiecutter_min_version_requirement = "2.0.1"
    if cookiecutter.__version__ < cookiecutter_min_version_requirement:
        logger.error(
            "Your Cookiecutter version: '"
            + cookiecutter.__version__
            + "'"
            + " does not meet the minimum requirement of '"
            + cookiecutter_min_version_requirement
            + "."
            + " Please upgrade to meet this requirement."
        )
        exit(1)


def _validate_project_name(project_name: str) -> None:
    """Validate entered project_name.

    Parameters
    ----------
    project_name
        Strong representing the project's name.
    """
    if not project_name.replace(" ", "").isalnum():
        logger.error(
            "The provided project_name '"
            + project_name
            + "' contains special characters."
            + " project_name must be composed solely of alphanumeric and space characters."
        )
        exit(1)


def _validate_project_slug(project_name: str, project_slug: str) -> None:
    """Validate resulting __projectslug.

    Parameters
    ----------
    project_name
        Strong representing the project's name.
    project_slug
        The Python package's name.
    """
    if project_slug == "" or not project_slug[0].isalpha():
        logger.error(
            "The provided project_name '"
            + project_name
            + "' resulted in the following invalid __project_slug: "
            + project_slug
            + "."
            + " __project_slug must start with an alphabetical character."
        )
        exit(1)


def main():
    """Enforce the project_slug to be viable and terminate if that is impossible."""
    project_name = "{{cookiecutter.project_name}}"
    project_slug = "{{cookiecutter.__project_slug}}"

    _validate_cookiecutter_version()
    _validate_project_name(project_name)
    _validate_project_slug(project_name, project_slug)


if __name__ == "__main__":
    _format_logging()
    main()
