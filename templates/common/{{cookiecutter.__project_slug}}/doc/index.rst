.. {{cookiecutter.project_name}} documentation master file

{{cookiecutter.project_name}}'s documentation
{% for _ in cookiecutter.project_name %}={% endfor %}================

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex`{% if cookiecutter.__project_type != "documentation" %} or :ref:`modindex`{% endif %}.
- Report bugs with {{cookiecutter.project_name}} in our `issue tracker <https://gitlab.com/{{cookiecutter.gitlab_group}}/{{cookiecutter.__project_slug}}/-/issues>`_.
- See this document as `pdf <{{cookiecutter.__project_slug}}.pdf>`_.

{%- if cookiecutter.__project_type != "documentation" %}

.. toctree::
   :maxdepth: 1
   :caption: First steps

   Installation <installation.rst>
   Usage <usage.rst>

{%- endif %}
{%- if cookiecutter.__project_type == "datascience" %}

.. toctree::
   :maxdepth: 1
   :caption: Algemeen

   Use case <use_case.rst>
   Privacy <privacy.rst>
   AI Impact Assessment <aiia.rst>

.. toctree::
   :maxdepth: 1
   :caption: Model

   Input <input.rst>
   Model <model.rst>
   Output <output.rst>

.. toctree::
   :maxdepth: 1
   :caption: Resultaten

   Resultaten <results.rst>

{%- endif %}

.. toctree::
   :maxdepth: 1
   :caption: All the rest
{% if cookiecutter.__project_type != "documentation" %}
   API <apidocs/{{cookiecutter.__package_name}}.rst>
{%- endif %}
   Contributing <contributing.rst>
   License <license.rst>
   Release notes <changelog.rst>
