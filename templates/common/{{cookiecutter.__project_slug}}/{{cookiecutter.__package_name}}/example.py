# -*- coding: utf-8 -*-
"""Documentation about the {{cookiecutter.__package_name}} module."""

import logging

logger = logging.getLogger(__name__)


# Put actual code here
def example() -> None:
    """My first example function.

    Returns
    -------
    None.

    """
    logger.info("Providing information about the excecution of the function.")
