# -*- coding: utf-8 -*-
"""Documentation about {{cookiecutter.project_name}}"""

import logging

logging.getLogger(__name__).addHandler(logging.NullHandler())

__author__ = "{{cookiecutter.author.replace('\"', '\\\"')}}"
__email__ = "{{cookiecutter.email}}"
__version__ = "{{cookiecutter.version}}"
